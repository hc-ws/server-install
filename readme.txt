This installer installs a hc.ws instance onto a FreeBSD 14.0 machine.
Alot of networking information is hardcoded, this repository is not designed to be used without source modification, trying to install this will likely break your machine.

pkg install -y vim git terminfo-db; git clone https://codeberg.org/hc-ws/server-install.git /tmp/server-install; cd /tmp/server-install/; ./install.sh