#!/bin/sh
# Variables
INV_USER="invidious"
INV_LOCATION="/home/$INV_USER/invidious"

# Secrets
DB_PASS="$(openssl rand -hex 64)"
HMAC_KEY="$(openssl rand -hex 16)"

SYNC_PORT="$((1000 + "0x$(openssl rand -hex 2)" % 65000))"
SYNC_USER="management"

# Get Configuration
read -p "Server Hostname: " SERVER_HOSTNAME # e.g. s0-us.hc.ws
read -p "Machine IPv4: " MACHINE_IP # e.g 000.000.000.000
read -p "Router IPv4: " ROUTER_IP
read -p "Machine IPv6: " MACHINE_IP6 # e.g 0000:0000:0000:0000::1
read -p "Router IPv6: " ROUTER_IP6

# System config
mv ./etc/rc.conf /etc/rc.conf
sed -i "" -e "
s#VAR_HOSTNAME#$SERVER_HOSTNAME#g;
s#VAR_IPV4_IP#$MACHINE_IP#g;
s#VAR_IPV4_ROUTER#$ROUTER_IP#g;
s#VAR_IPV6_IP#$MACHINE_IP6#g;
s#VAR_IPV6_ROUTER#$ROUTER_IP6#g
" /etc/rc.conf

# Create management user
pw useradd -n $SYNC_USER -m -s /bin/sh -G www
mkdir /home/$SYNC_USER/.ssh/
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHBJIvFkKX10hNqD5HQBfDuy0z5gBJmbDkpXYN36jCAZ umbreon_management" > /home/$SYNC_USER/.ssh/authorized_keys
sed -i "" -e "s#\#Port 22Port $SYNC_PORT#g" /etc/ssh/sshd_config

# Install Nginx
pkg install -y nginx
mv ./usr/local/etc/nginx/nginx.conf /usr/local/etc/nginx/nginx.conf
mkdir -m 0770 /usr/local/etc/nginx/certs /usr/local/etc/nginx/certs/hc.ws /usr/local/etc/nginx/certs/y.hc.ws
chown -R www:www /usr/local/etc/nginx/certs
touch /var/log/invidious
mv ./www/* /usr/local/www

# Install Invidious
pkg install -y crystal shards sqlite3 postgresql16-client postgresql16-server
pw useradd -n $INV_USER -m -s /sbin/nologin
git clone https://codeberg.org/hc-ws/invidious.git $INV_LOCATION
chown -R $INV_USER:$INV_USER $INV_LOCATION

# Update config files
mv ./config.yml $INV_LOCATION/config/config.yml
sed -i "" -e "s#VAR_DB_PASS#$DB_PASS#g;s#VAR_HMAC_KEY#$HMAC_KEY#g" $INV_LOCATION/config/config.yml

# Setup database
service postgresql initdb
service postgresql start
su -l postgres -c "psql -c \"CREATE USER kemal WITH PASSWORD '$DB_PASS';\""
su -l postgres -c "createdb -O kemal invidious"

# Build Invidious
su -m $INV_USER -c "cd $INV_LOCATION; make;"

# Move resources
mkdir "/var/run/invidious" "/usr/local/etc/rc.d/" "/usr/local/etc/syslog.d/" "/usr/local/etc/cron.d/"
mv ./usr/local/etc/rc.d/* /usr/local/etc/rc.d/
mv ./usr/local/etc/syslog.d/* /usr/local/etc/syslog.d/
mv ./usr/local/etc/cron.d/* /usr/local/etc/cron.d/
mv ./usr/local/bin/* /usr/local/bin/
sed -i "" -e "s#VAR_IPV6_PRE#$(echo $MACHINE_IP6 | sed 's/\:\:.*//g')#g" /usr/local/bin/ipv6-rotate

# Restart Services
ssh-keygen -A
service sshd restart

# Finished
echo "+-------------------------------------------------+"
echo "| Finished installing, restart to complete setup. |"
echo "|                                                 |"
echo "+----------------| Configuration |----------------|"
echo "| SSH Port: '$SYNC_PORT'                               |"
echo "+-------------------------------------------------+"
